package ru.rt;

import java.util.Objects;

public class Edge {

    public int v1;
    public int v2;
    public Edge(int v1, int v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return (v1 == edge.v1 && v2 == edge.v2) || (v1 == edge.v2 && v2 == edge.v1);
    }

    @Override
    public int hashCode() {
        return Objects.hash(v1, v2);
    }

    @Override
    public String toString() {
        return "\n  Edge{" +
                "v1=" + v1 +
                ", v2=" + v2 +
                "}";
    }
}
