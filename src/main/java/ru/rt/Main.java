package ru.rt;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws Exception {
        int[][] graph = new int[][]
                {
                        {0,7,0,5,0,0,0},
                        {7,0,8,9,7,0,0},
                        {0,8,0,0,5,0,0},
                        {5,9,0,0,15,6,0},
                        {0,7,5,15,0,8,9},
                        {0,0,0,6,8,0,11},
                        {0,0,0,0,9,11,0}

                };
        Kraskal kar = new Kraskal(graph);
        System.out.println(Arrays.deepToString(kar.calc()));
    }

}
