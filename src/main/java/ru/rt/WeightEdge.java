package ru.rt;

import java.util.Objects;

public class WeightEdge implements Comparable{
    Edge edge;
    int weight = 0;

    public WeightEdge(int weight, Edge edge) {
        this.edge = edge;
        this.weight = weight;
    }

    @Override
    public int compareTo(Object o) {
        if(this.weight > ((WeightEdge)o).weight){
            return 1;
        }
        if(this.weight < ((WeightEdge)o).weight){
            return -1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "WeightEdge{" +
                "edge=" + edge +
                ", weight=" + weight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeightEdge that = (WeightEdge) o;
        return weight == that.weight && Objects.equals(edge, that.edge);
    }

    @Override
    public int hashCode() {
        return Objects.hash(edge, weight);
    }
}
