package ru.rt;

public class Kraskal {
    private int [][] table;
    private SingleArray<WeightEdge> weights;
    private Edge[] result = new Edge[0];
    private SingleArray used;
    private SingleArray usedEdges;

    public Kraskal(int [][] table){
        this.table = table;
        this.weights = new SingleArray();
        this.used = new SingleArray();
        this.usedEdges = new SingleArray();
        findWeights();
    }

    private void findWeights() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                Integer i1 = table[i][j];
                if(i1 != 0){
                    WeightEdge weightEdge = new WeightEdge(i1, new Edge(i, j));
                    if(!weights.contains(weightEdge)){
                        weights.add(weightEdge);
                    }
                }
            }
        }
        SingleArray<WeightEdge> sorted = new HeapSort<>(weights).sort();
        this.weights = sorted;
    }

    public Edge[] calc(){
        for (int i = 0; i < weights.size(); i++) {
            WeightEdge o = weights.get(i);
            if(used.contains(o.edge.v1) && used.contains(o.edge.v2)){
                boolean res = dfs(o.edge.v1, o.edge.v2);
                usedEdges = new SingleArray();
                if(res){
                    continue;
                }
            }
            used.add(o.edge.v1);
            used.add(o.edge.v2);
            add(o.edge);
        }
        return result;
    }

    private boolean dfs(int v, int find){
        if(v == find) return true;
        boolean res = false;
        for (int i = 0; i < result.length; i++) {
            Edge elem = result[i];
            if(usedEdges.contains(elem))continue;
            if(elem.v1 == v){
                usedEdges.add(elem);
                res = dfs(elem.v2,find);
            }else if(elem.v2 == v ){
                usedEdges.add(elem);
                res = dfs(elem.v1,find);
            }
            if(res == true) break;
        }
        return res;
    }

    private void add(Edge edge) {
        Edge[] nArr = new Edge[result.length+1];
        for (int i = 0; i < result.length; i++) {
            nArr[i]=result[i];
        }
        nArr[nArr.length-1] = edge;
        result = nArr;
    }
}
