package ru.rt;

public class HeapSort<T> extends Sorter{
    HeapSort(SingleArray<T> arr) {
        super(arr);
    }

    @Override
    public SingleArray<T> sort() {
        for (int root = arr.size()/2-1; root >= 0 ; root--) {
            moveMaxToRoot(root,arr.size());
        }

        for (int i = arr.size() - 1; i >=0 ; i--) {
            swap(0,i);
            moveMaxToRoot(0,i);
        }
        return arr;
    }

    void moveMaxToRoot(int root, int size){
        int l = 2*root + 1;
        int r = 2*root + 2;

        int x = root;
        if(l < size && ((Comparable)arr.get(x)).compareTo(arr.get(l)) < 0) x = l;
        if(r < size && ((Comparable)arr.get(x)).compareTo(arr.get(r)) < 0) x = r;

        if(x == root) return;

        swap(x,root);
        moveMaxToRoot(x,size);

    }
}
