package ru.rt;

public class SimpleQueue<T> {
    T value;
    SimpleQueue next;

    SimpleQueue(){
        this.value = null;
        next = null;
    }

    SimpleQueue(T value){
        this.value = value;
        next = null;
    }

    public void enqueue(T value){
        if(empty()){
            this.value = value;
        }else if(next == null){
            next = new SimpleQueue(value);
        }else {
            next.enqueue(value);
        }

    }

    public T dequeue(){
        if(next == null){
            T value = this.value;
            this.value = null;
            return value;
        }
        if(next.next == null){
            T value = ((SimpleQueue<T>)next).value;
            this.next = null;
            return value;
        }
        return ((SimpleQueue<T>)next).dequeue();
    }

    public boolean empty() {
        return this.next == null && this.value == null;
    }

    @Override
    public String toString() {
        return "SimpleQueue{" +
                "value=" + value +
                ", next=" + next +
                '}';
    }
}
