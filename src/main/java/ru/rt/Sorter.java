package ru.rt;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class Sorter<T>{

    protected int size;
    protected SingleArray<T> arr;

    Sorter(SingleArray<T> arr){
        this.arr = arr;
        this.size = arr.size();
    }

    abstract SingleArray<T> sort();

    public void swap(int i, int j){
        T x = arr.get(i);
        arr.put(arr.get(j),i);
        arr.put(x,j);
    }
}
